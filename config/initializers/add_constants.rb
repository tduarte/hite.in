APP_DOMAIN = Rails.env == 'production' ? "hite.in" : "hite.in"
APP_PORT = Rails.env == 'production' ? "" : ":3000"
APP_HOST = APP_DOMAIN + APP_PORT

APP_NAME = "Bloat Me"